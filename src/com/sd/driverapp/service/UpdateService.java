package com.sd.driverapp.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.sd.driverapp.util.Constants;



public class UpdateService  extends Service implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener{

	private boolean runService=true;
	private Handler handler=new Handler();
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	public static double myLatitude = 0.0;
	public static double myLongitude = 0.0;
	private LocationClient locationclient;
	private boolean isconnected = false;
	private Location mylocation;
	private LocationRequest locationrequest;
	LocationManager locationManager;
	LocationListener locationListener=new MyLocationListner();
	@Override
	public IBinder onBind(Intent intent) {
		Log.e("MSG","IBinderCalled");
		return null;
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e("MSG","onstartCommandCalled");
	//	handler.post(timeTask);
		runService=intent.getBooleanExtra("start",true);
		LoactionTrack();
		return super.onStartCommand(intent, flags, startId);
	}
	private Runnable timeTask=new Runnable() {
		
		@Override
		public void run() {
			Log.e("message","running");
			if(runService)
				handler.postDelayed(timeTask,Constants.TIME_INTERVAL);
			else
				stopSelf();
			
		}
	};
	public void onDestroy() {
		Log.e("message","ondestry");
		locationclient.removeLocationUpdates(locationListener);
	/*	if (isconnected)
			if (locationclient.isConnected())
			{
				
				//locationclient.disconnect();
				//LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				Log.e("message","disconnect");
			}*/
		super.onDestroy();
	}
	private void LoactionTrack()
	{
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (hasInternet(getApplicationContext())) {
			if (!manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

				/*Toast.makeText(this, "Please turn On Location Services",
						Toast.LENGTH_LONG).show();*/
				//showNoLocationDialog();

			} else
				startLoacationTrack();
		}
		else
			Toast.makeText(this, "No internet connection",
					Toast.LENGTH_LONG).show();
	}
		public static boolean hasInternet(Context context) {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}
					}
				}
			}
			return false;
		}

		public void startLoacationTrack() {
			// location Data
			Log.e("start","start location track");
			locationclient = new LocationClient(this, this, this);
			locationrequest = LocationRequest.create();
			// Use high accuracy
			locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			// Set the update interval to 10 seconds
			locationrequest.setInterval(10000);
			// Set the fastest update interval to 3 second
			locationrequest.setFastestInterval(3000);
			locationclient.connect();
			
		}
	
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}
	@Override
	public void onConnected(Bundle arg0) {
		Log.e("msg","connected");
		mylocation = locationclient.getLastLocation();
		isconnected=true;
		
		locationclient.requestLocationUpdates(locationrequest,locationListener);
		
	}
	@Override
	public void onDisconnected() {
		
		
	}
	private class AddMenuPost extends AsyncTask<Void,Void,Void>
	{
		String id;
		String Lat;;
		String lon;
		long time;
		InputStream IS;
		String msg="";
		String fileName="addlocation.php";
		String url=Constants.URL_ROOT+fileName;
		public AddMenuPost(String id,String lat,String lon )
		{
			this.id=id;
			this.Lat=lat;
			this.lon=lon;
			this.time=System.currentTimeMillis();
			
		}
		@Override
		protected Void doInBackground(Void... params) {
			//String url = "http://travelaid.net78.net/addlocation.php";

			HttpClient clinet = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("id", "" +Constants.BUSID));
			key.add(new BasicNameValuePair("lat", ""+Lat));
			key.add(new BasicNameValuePair("lon", ""+lon));
			key.add(new BasicNameValuePair("time", ""+time));



			try {
				httppost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				HttpResponse response = clinet.execute(httppost);
				HttpEntity entity = response.getEntity();
				IS = entity.getContent();

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			BufferedReader bf = new BufferedReader(new InputStreamReader(IS));
			String line = "";

			try {
				while ((line = bf.readLine()) != null) {
					msg = msg + line;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			msg = msg.substring(0, msg.length() - 146);
			
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			//Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG).show();
			//setUiClean();
			Log.e("msg",msg);
			super.onPostExecute(result);
		}
		
	}
	private class MyLocationListner implements LocationListener
	{
		@Override
		public void onLocationChanged(Location location) {
			Log.e("msg","location changed");
			myLatitude = location.getLatitude();
			myLongitude = location.getLongitude();
			new AddMenuPost("1",""+myLatitude,""+myLongitude).execute();
			if(!runService)
			{
				stopSelf();
			}
			
		}
		
	}

	

}
