package com.sd.driverapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sd.driverapp.service.UpdateService;
import com.sd.driverapp.util.Constants;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	private Button btnStart, btnStop;
	private ProgressBar pbBar;
	private TextView tvStatus;
	private String iMEI="";
	private ProgressDialog pdDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.activity_main);
		
		btnStart = (Button) findViewById(R.id.btn_start);
		btnStop = (Button) findViewById(R.id.btn_stop);
		pbBar=(ProgressBar)findViewById(R.id.progressBar);
		tvStatus=(TextView)findViewById(R.id.tv_visibilty);
		btnStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Intent intent = new Intent(getApplicationContext(), UpdateService.class);
				// startService(intent);
				
				checkLocationProviderEnabled();
				

			}
		});
		btnStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), UpdateService.class);
				intent.putExtra("start", false);
				stopService(intent);
				Constants.isServiceRunning=false;
				setUIVisibility();
			}
		});
		setUIVisibility();
	}
	private void getIMEIAndBusInfo()
	{
		if(Constants.hasInternet(MainActivity.this))
		{
		getIMEINumber();
		getBusInfo();
		}
		else
			internetAvailabilityAlert(MainActivity.this,"Please Check your internet connection.");
	}
	private void checkLocationProviderEnabled()
	{
		 LocationManager manager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		 if(!manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
		 {
			 	Log.e("MSG","Location Provider Disabled.");
	            LocationProviderDisableAlert(MainActivity.this,"Please Enable Location Provider.");
			 
		 }
		 else
			 getIMEIAndBusInfo();
	}
	private void setUIVisibility()
	{
		if(Constants.isServiceRunning)
		{
			tvStatus.setVisibility(View.VISIBLE);
			pbBar.setVisibility(View.VISIBLE);
			btnStop.setVisibility(View.VISIBLE);
			btnStart.setVisibility(View.INVISIBLE);
			
		}
		else
		{
			tvStatus.setVisibility(View.INVISIBLE);
			pbBar.setVisibility(View.INVISIBLE);
			btnStop.setVisibility(View.INVISIBLE);
			btnStart.setVisibility(View.VISIBLE);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void getIMEINumber() {
		TelephonyManager mngr = (TelephonyManager) getSystemService(this.TELEPHONY_SERVICE);
		iMEI = mngr.getDeviceId();
		Log.e("MSG", iMEI);
	}

	private void getBusInfo() {
			//showBusInfoDialog();
		Log.e("MSG","Getting Bus Info...");
		pdDialog=Constants.createProgressDialog(MainActivity.this);
		pdDialog.show();
		new getBusInfoFromIMEI().execute();
	}

	private void showBusInfoDialog(String name,String route) {
		final Dialog infoDialog = new Dialog(this);
		infoDialog.setContentView(R.layout.dialog_businfo);
		infoDialog.setTitle("Bus Info");
		Button btnStart = (Button) infoDialog.findViewById(R.id.button1);
		Button btnCancel = (Button) infoDialog.findViewById(R.id.button2);
		TextView tvName=(TextView)infoDialog.findViewById(R.id.tv_Name);
		TextView tvRoute=(TextView)infoDialog.findViewById(R.id.tv_route);
		tvName.setText(name);
		tvRoute.setText(route);
		btnStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				infoDialog.dismiss();
				Intent intent = new Intent(getApplicationContext(), UpdateService.class);
				startService(intent);
				Constants.isServiceRunning=true;
				setUIVisibility();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				infoDialog.dismiss();
				
			}
		});
		infoDialog.show();
	}
	private class getBusInfoFromIMEI extends AsyncTask<Void,Void,Void>
	{
		String fileName="get_bus_info_from_imei.php";
		String url=Constants.URL_ROOT+fileName;
		String msg="";
		InputStream IS;

		@Override
		protected Void doInBackground(Void... params) {
			HttpClient clinet = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("imei", "" +iMEI));



			try {
				httppost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				HttpResponse response = clinet.execute(httppost);
				HttpEntity entity = response.getEntity();
				IS = entity.getContent();

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			BufferedReader bf = new BufferedReader(new InputStreamReader(IS));
			String line = "";

			try {
				while ((line = bf.readLine()) != null) {
					msg = msg + line;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			msg = msg.substring(0, msg.length() - 146);
			
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			ParseJson(msg);
			pdDialog.dismiss();
			super.onPostExecute(result);
		}
		private void ParseJson(String json)
		{
			try {
				JSONObject jobj=new JSONObject(json);
				JSONObject obj=jobj.getJSONObject("success");
				Constants.BUSID=obj.getInt("bus_id");
				String name=obj.getString("service_id");
				String start=obj.getString("start");
				String end=obj.getString("end");
				showBusInfoDialog(name, start+"_"+end);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

	private void internetAvailabilityAlert(final Context context, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
			}
		});
		bld.setNegativeButton("Retry", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				getIMEIAndBusInfo();
				
			}
		});

		bld.create().show();
	}
	private void LocationProviderDisableAlert(final Context context, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
			}
		});
		bld.setNegativeButton("Enable", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				//to do
				
	            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
	            startActivityForResult(myIntent, Constants.REQUEST_PROVIDER_ENABLING);
			}
		});

		bld.create().show();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==Constants.REQUEST_PROVIDER_ENABLING)
		{
			checkLocationProviderEnabled();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
